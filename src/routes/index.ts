import { Router } from 'express';

import sectionsRouter from './sections.routes';

const routes = Router();

routes.get('/', (reqquest, response) => {
  response.json({
    applicationName: 'Ponto Eletrônico',
    status: 'ok',
    version: '1.0.0',
  });
});

routes.use('/sections', sectionsRouter);

export default routes;
