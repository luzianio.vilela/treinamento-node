import { EntityRepository, Repository } from 'typeorm';
import Section from '../models/Section';

@EntityRepository(Section)
class SectionsRepository extends Repository<Section> {
  public async findById(id: string | undefined): Promise<Section | undefined> {
    const findSectionById = await this.findOne({
      where: { id },
    });

    return findSectionById;
  }

  public async findByDescription(
    description: string | undefined,
  ): Promise<Section | undefined> {
    const findDescription = await this.findOne({
      where: { description },
    });

    return findDescription;
  }
}

export default SectionsRepository;
