import { getCustomRepository } from 'typeorm';

import SectionsRepository from '../repositories/SectionsRepository';
import Section from '../models/Section';

interface IRequest {
  id: string;
  description: string;
  number: string;
  state: string;
  active: boolean;
}

class UpdateSectionService {
  public async execute({
    id,
    description,
    number,
    state,
    active,
  }: IRequest): Promise<Section> {
    const sectionsRepository = getCustomRepository(SectionsRepository);

    const section = await sectionsRepository.findById(id);

    if (!section) {
      throw Error('Lotação não encontrada');
    }

    const sectionWithUpdatedDescription = await sectionsRepository.findByDescription(
      description,
    );

    // Verifica se já existe uma lotacao com este nome e que seja diferente
    // da lotacao que estamos alterando.
    if (
      sectionWithUpdatedDescription &&
      sectionWithUpdatedDescription.id !== id
    ) {
      throw Error('Já existe uma lotação com este nome');
    }

    Object.assign(section, { description, number, state, active });

    await sectionsRepository.save(section);

    return section;
  }
}

export default UpdateSectionService;
