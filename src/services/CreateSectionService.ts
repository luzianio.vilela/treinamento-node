import { getCustomRepository } from 'typeorm';

import SectionsRepository from '../repositories/SectionsRepository';
import Section from '../models/Section';

interface IRequest {
  id: string;
  description: string;
  number: string;
  state: string;
}

class CreateSectionService {
  public async execute({
    id,
    description,
    number,
    state,
  }: IRequest): Promise<Section> {
    const sectionsRepository = getCustomRepository(SectionsRepository);

    const findSectionWithSameDescription = await sectionsRepository.findByDescription(
      description,
    );

    if (findSectionWithSameDescription) {
      throw Error('Já existe uma lotação com essa descrição');
    }

    const section = sectionsRepository.create({
      id,
      description,
      number,
      state,
      active: true,
    });

    await sectionsRepository.save(section);

    return section;
  }
}

export default CreateSectionService;
